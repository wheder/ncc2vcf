<?php

class NCC2PDF {
    const HEADER = "BEGIN:VCARD\r\nVERSION:3.0\r\n";
    const FOOTER = "END:VCARD\r\n";
  
    public static function getTypes() {
        return Array(
            202 => 'FN',
            208 => 'TEL;TYPE=CELL', //general phone
            209 => 'TEL;TYPE=HOME',
            210 => 'TEL;TYPE=CELL',
            213 => 'TEL;TYPE=WORK',
            219 => 'TEL;TYPE=CELL', //fax
            205 => 'EMAIL;TYPE=INTERNET;TYPE=WORK',
            204 => 'NOTE',
            217 => null, //CALLER_GROUP_REF
            206 => 'NOTE', //address
            
            //control
            200 => null, //PIT_CONTACT
            225 => null, //PIT_CONTACT_SIM
            
            //banned
            300 => null, //PIT_CALLERGROUP
            308 => null,
            301 => null,
            302 => null,
            303 => null,
            304 => null,
            306 => null,
            307 => null,
        );
    
    
    }
    
    public static function convert($nccString) {
        $rows = self::getRows($nccString);
        $data = self::parseRows($rows);
        return self::compile($data);
    }
    
    private static function getRows($input) {
        return explode("\r\n", $input);
    }
    
    private static function parseRows(Array $in) {
        $out = Array();
        foreach ($in as $item) {
            $retItem = Array();
            $tmpFields = explode("\t", $item);
            for ($i = 0; $i < sizeof($tmpFields); $i = $i+2 ) {
                $k = null;
                $v = null;
                $k = $tmpFields[$i];
                if (array_key_exists($i+1, $tmpFields)) {
                    $v = $tmpFields[$i+1];
                }
                if ($k !== null) {
                    $r = self::translate($k, $v);
                    if ($r !== null) {
                        $retItem[] = $r;
                    }
                }
            }
            if ($retItem !== Array()) {
              $out[] = $retItem;
            }
        }
        
        return $out;
    }
    
    private static function translate($k, $v) {
        $t = self::getTypes();
        if (!array_key_exists($k, $t)) {
            trigger_error('Unknown key '.$k, E_USER_WARNING);
            return null;
        }
        if ($t[$k] === null) {
            return null;
        }
        return $t[$k].':'.$v."\r\n";
    }
    
    
    private static function compile(Array $data) {
        $ret = '';
        
        foreach ( $data as $item ) {
            $ret .= self::HEADER . implode('', $item) . self::FOOTER;
        }
        
        return $ret;
    }


}



$path = '/mnt/c/Users/XXX/Desktop/PhoneBook.ncc';
$a = file_get_contents($path);
$x = substr($a, 3); //bom

//echo $x;

$f = NCC2PDF::convert($a);


file_put_contents($path.'.vcf', $f);
var_dump($f);

